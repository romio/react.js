var possibleCombinationSum = function(arr, n) {
    if (arr.indexOf(n) >= 0) { return true; }
    if (arr[0] > n) { return false; }
    if (arr[arr.length - 1] > n) {
        arr.pop();
        return possibleCombinationSum(arr, n);
    }
    var listSize = arr.length, combinationsCount = (1 << listSize);
    for (var i = 1; i < combinationsCount ; i++ ) {
        var combinationSum = 0;
        for (var j=0 ; j < listSize ; j++) {
            if (i & (1 << j)) { combinationSum += arr[j]; }
        }
        if (n === combinationSum) { return true; }
    }
    return false;
};

var StarsFrame = React.createClass({
    render: function() {
        var stars = [];
        for (var i = 0; i<this.props.numberOfStars; i++) {
            stars.push(<span className="glyphicon glyphicon-star"></span>);
        }
        return (
            <div id="stars-frame">
                <div className="well">
                    {stars}
                </div>
            </div>
        )
    }
});

var ButtonFrame = React.createClass({
    render: function() {

        var disabled,
            correct = this.props.correct,
            redraws = this.props.redraws,
            checkAnswer = this.props.checkAnswer,
            button;

        switch (correct) {
            case true:
                button = (<button className="btn btn-success btn-lg"  onClick={this.props.acceptAnswer}>
                    <span className="glyphicon glyphicon-ok"></span>
                </button>);
                break;
            case false:
                button = (<button className="btn btn-danger btn-lg">
                    <span className="glyphicon glyphicon-remove"></span>
                </button>);
                break;
            default:
                disabled = this.props.selectedNumbers.length === 0;
                button = (<button className="btn btn-primary btn-lg" disabled={disabled} onClick={checkAnswer}>=</button>)
        }

        return (
            <div id="button-frame">
                {button}
                <br /><br />
                <button className="btn btn-warning btn-xs" onClick={this.props.redraw} disabled={redraws === 0}>
                    <span className="glyphicon glyphicon-refresh"></span>
                    &nbsp;
                    {redraws}
                </button>
            </div>
        )
    }
});

var AnswerFrame = React.createClass({
    render: function() {
        var props = this.props;
        var selectedNumbers = props.selectedNumbers.map(function (number) {
            return (
                <span onClick={props.unselectNumber.bind(null, number)}>{number}</span>
            )
        });
        return (
            <div id="answer-frame">
                <div className="well">{selectedNumbers}</div>
            </div>
        )
    }
});

var NumbersFrame = React.createClass({
    render: function() {
        var numbers = [],
            className,
            selectedNumbers = this.props.selectedNumbers,
            usedNumbers = this.props.usedNumbers,
            selectNumber = this.props.selectNumber;

        for (var i=1; i<10; i++) {
            className = 'number selected-' + (selectedNumbers.indexOf(i) >= 0);
            className += ' used-' + (usedNumbers.indexOf(i) >= 0);
            numbers.push(<div className={className} onClick={selectNumber.bind(null, i)}>{i}</div>)
        }
        return (
            <div id="numbers-frame">
                <div className="well">
                    {numbers}
                </div>
            </div>
        )
    }
});

var DoneFrame = React.createClass({
    render: function() {
        return (
            <div id="done-frame">
                <div className="well text-center">
                    <h2>{this.props.doneStatus}</h2>
                    <button className="btn btn-default" onClick={this.props.resetGame}>Play again</button>
                </div>
            </div>
        )
    }
});


var Game = React.createClass({
    getInitialState: function() {
        return {
            numberOfStars: this.getRandomNumberOfStars(),
            selectedNumbers: [],
            usedNumbers: [],
            redraws: 5,
            doneStatus: 'lol',
            correct: null
        }
    },
    selectNumber: function(num) {
        if (this.state.selectedNumbers.indexOf(num)<0) {
            this.setState({selectedNumbers:this.state.selectedNumbers.concat(num), correct:null})
        }

    },
    unselectNumber: function(num) {
        var selectedNumbers = this.state.selectedNumbers,
            index = selectedNumbers.indexOf(num);
        selectedNumbers.splice(index, 1);

        this.setState({selectedNumbers: selectedNumbers, correct:null});
    },
    sumOfSelectedNumbers: function() {
        return this.state.selectedNumbers.reduce(function(p, n) {
            return p+n;
        }, 0);
    },
    checkAnswer: function () {
        var correct = this.state.numberOfStars === this.sumOfSelectedNumbers();
        this.setState({correct: correct});
    },
    acceptAnswer: function () {
        var usedNumbers = this.state.usedNumbers.concat(this.state.selectedNumbers);
        this.setState({
            usedNumbers: usedNumbers,
            selectedNumbers: [],
            correct: null,
            numberOfStars: this.getRandomNumberOfStars()
        }, function() {
            this.updateDoneStatus();
        })
    },
    getRandomNumberOfStars: function() {
        return 1 + Math.floor(Math.random()*9);
    },
    redraw: function() {
        if (this.state.redraws > 0) {
            this.setState({
                numberOfStars: this.getRandomNumberOfStars(),
                correct: null,
                selectedNumbers: [],
                redraws: this.state.redraws - 1
            }, function() {
                this.updateDoneStatus();
            })
        }
    },
    updateDoneStatus: function() {
        if (this.state.usedNumbers.length === 9) {
            this.setState({
                doneStatus: 'Victory!'
            });
            return;
        }
        if (this.state.redraws === 0 && !this.possibleSolutions()) {
            this.setState({
                doneStatus: 'Game over!'
            });
        }
    },
    possibleSolutions: function() {
        var numberOfStars = this.state.numberOfStars,
            usedNumbers = this.state.usedNumbers,
            possibleNumbers = [];

        for (var i = 1; i<10; i++) {
            if (usedNumbers.indexOf(i) < 0) {
                possibleNumbers.push(i);
            }
        }

        return possibleCombinationSum(possibleNumbers, numberOfStars);
    },
    resetGame: function() {
        this.replaceState(this.getInitialState());
    },
    render: function() {
        var selectedNumbers = this.state.selectedNumbers,
            usedNumbers = this.state.usedNumbers,
            numberOfStars = this.state.numberOfStars,
            redraws = this.state.redraws,
            doneStatus = this.state.doneStatus,
            correct = this.state.correct,
            bottomFrame = doneStatus ?
                <DoneFrame doneStatus={doneStatus} resetGame={this.resetGame} />
                :
                <NumbersFrame selectedNumbers={selectedNumbers}
                              usedNumbers={usedNumbers}
                              selectNumber={this.selectNumber} />;

        return (
            <div id="game">
                <h2>Play Nine</h2>
                <hr />
                <div className="clearfix">
                    <StarsFrame numberOfStars={numberOfStars} />
                    <ButtonFrame selectedNumbers={selectedNumbers}
                                 correct={correct}
                                 checkAnswer={this.checkAnswer}
                                 acceptAnswer={this.acceptAnswer}
                                 redraw={this.redraw}
                                 redraws={redraws}/>
                    <AnswerFrame selectedNumbers={selectedNumbers}
                                 unselectNumber={this.unselectNumber} />
                </div>
                {bottomFrame}
            </div>
        )
    }
});

ReactDOM.render(<Game />, document.getElementById("container"));